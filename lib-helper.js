// This simple script makes your live easer
// It copys the main README.md to the lib directory and updates the git version to the two package.json's

const execSy = require('child_process').execSync
const { promises: fsp } = require("fs");
const fs = require('fs')

const gitVersion = execSy("git describe").toString().trim()

console.log('##### lib-helper started #####')

async function getFiles(path = './', filename = 'package.json') {
    const entries = await fsp.readdir(path, { withFileTypes: true });

    // Get files within the current directory and add a path key to the file objects
    const files = entries
        .filter(file => !file.isDirectory() && file.name == filename)
        .map(file => ({ ...file, path: path + file.name }));

    // Get folders within the current directory
    const folders = entries.filter(folder => folder.isDirectory() && folder.name != 'node_modules' && folder.name != 'dist');

    for (const folder of folders)
        /*
          Add the found files within the subdirectory to the files array by calling the
          current function itself
        */
        files.push(...await getFiles(`${path}${folder.name}/`));

    return files;
}

getFiles().then(files => {
    files.forEach(fileStats => {
        let package = require(fileStats.path)
        package.version = gitVersion
        fs.writeFile(fileStats.path, JSON.stringify(package, null, 2), function (err) {
            if (err) throw err;
            console.log(` --> The new version: ${gitVersion} was patched to ${fileStats.path}`);
        })
    })
})
