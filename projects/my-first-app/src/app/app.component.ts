import { Component, OnInit } from '@angular/core';
import { CalendarConfig, DayC } from 'projects/material-calendar/src/public-api';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'Material Calendar Demo';

  isLoading = true
  placeholder = false // boolean

  monthsAfterBefore = Array(5).fill(0).map((x, i) => i);
  monthsBefore = 0;
  monthsAfter = 0;

  calendarConfig: CalendarConfig = {
    renderMode: 'monthly', // 'annual' | 'monthly'
    selectMode: 'range',  // 'click' | 'range'
    displayYear: false,
    firstDayOfWeekMonday: true,
    calendarWeek: true,
    switches: true,
    panelWidth: '30%'
  }

  dataSource: DayC[] = [
    {
      date: 1604185200000,
      backgroundColor: 'rgba(0, 0, 255, 0.5)',
      toolTip: 'Test ToolTip',
      badgeMode: 'Icon',
      badgeInt: 5,
      badgeIcon: 'edit'
    },
    {
      date: 1604185200000,
      backgroundColor: 'rgba(0, 255, 0, 0.5)',
      toolTip: 'Test ToolTip 2',
      badgeMode: 'Icon',
      badgeInt: 5,
      badgeIcon: 'edit'
    },
    {
      date: 1604199900000,
      backgroundColor: 'blue'
    }
  ]

  constructor() {
  }

  ngOnInit() {
    console.log(this.dataSource)
    this.isLoading = false
  }

  testMethod(event) {
    console.log(event)
  }
}
