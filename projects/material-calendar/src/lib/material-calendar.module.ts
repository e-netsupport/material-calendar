import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatTooltipModule } from '@angular/material/tooltip';

import { CalendarPanelComponent } from './calendar-panel/calendar-panel.component';

@NgModule({
  declarations: [
    CalendarPanelComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatIconModule,
    MatButtonModule,
    MatTooltipModule
  ],
  exports: [
    CalendarPanelComponent
  ]
})
export class MaterialCalendarModule { }
